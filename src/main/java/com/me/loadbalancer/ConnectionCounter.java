package com.me.loadbalancer;

public class ConnectionCounter {

    private int count;

    public synchronized void increment(){
        count++;
    }

    public synchronized void decrement(){
        count--;
    }

    public synchronized void reset(){
        count = 0;
    }

    public synchronized int getCount(){
        return count;
    }
}
