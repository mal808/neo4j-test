package com.me.loadbalancer;

import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.timeout.IdleStateEvent;

import java.util.concurrent.ConcurrentHashMap;

public class LoadBalancerChannelDuplexHandler extends ChannelDuplexHandler {

    private ConcurrentHashMap<Integer, Server> serverMap;

    public LoadBalancerChannelDuplexHandler(ConcurrentHashMap<Integer, Server> serverMap){
        this.serverMap = serverMap;
    }

    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object event){
       if(event instanceof IdleStateEvent){
         IdleStateEvent e = (IdleStateEvent) event;
         serverMap.forEach((x,y) -> {
             if(y.containsConnection((ctx.channel()))){
                 System.out.println("Closing server " + y.getName() + " which has " + y.getCount() + " connections on timeout");
                 y.closeAllChannels();
                 y.resetCount();
             }
         });
       }
    }
}
