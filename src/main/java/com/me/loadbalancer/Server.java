package com.me.loadbalancer;

import com.me.loadbalancer.ConnectionCounter;
import io.netty.channel.Channel;

import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents one of the servers we are load balancing
 * Contains information about the number of established connections,
 * Channel references, com.me.loadbalancer.Server name and InetSocketAddress
 */
public class Server {

    private final String name;
    private final InetSocketAddress inetSocketAddress;
    private final ConnectionCounter counter;
    private final List<Channel> channelList;

    public Server(String name, InetSocketAddress inetSocketAddress){
        this.name = name;
        this.inetSocketAddress = inetSocketAddress;
        this.counter = new ConnectionCounter();
        this.channelList = new ArrayList();
    }

    public String getName(){
        return name;
    }

    public InetSocketAddress getInetSocketAddress(){
        return inetSocketAddress;
    }

    public void incrementCount(){
        counter.increment();
    }

    public void decrementCount(){
        counter.decrement();
    }

    public int getCount(){
        return counter.getCount();
    }

    public void resetCount(){
        counter.reset();
    }

    public void addConnection(Channel channel){
        channelList.add(channel);
    }

    public boolean containsConnection(Channel channel){
        return channelList.contains(channel);
    }

    public void closeAllChannels(){
        channelList.forEach(Channel::close);
    }
}
