package com.me.loadbalancer;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.timeout.IdleStateHandler;

import java.net.InetSocketAddress;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class LoadBalancer {

    /*
     * My intention was to build simple servers to query, but I didn't have time.
     * I originally planned on using the port number as the ID since they would all be running locally.
     * I kept these Server objects and static server ID's in to show how I was thinking, even though they
     * weren't actually used as expected in the end.
     */
    public static final int SERVER_A = 50000;
    public static final int SERVER_B = 50001;
    public static final int SERVER_C = 50002;
    public static final int SERVER_D = 50003;
    public static final int SERVER_E = 50004;
    public static final int SERVER_F = 50005;
    public static final int SERVER_G = 50006;

    private final Random random;

    private int port;
    private ConcurrentHashMap<Integer, Server> serverMap;


    public LoadBalancer(int port) {
        this.port = port;
        System.out.println("Creating serverMap");
        serverMap = new ConcurrentHashMap<Integer, Server>();

        System.out.println("Creating server list");
        serverMap.put(SERVER_A, new Server("a", new InetSocketAddress("127.0.0.1", SERVER_A)));
        serverMap.put(SERVER_B, new Server("b", new InetSocketAddress("127.0.0.1", SERVER_B)));
        serverMap.put(SERVER_C, new Server("c", new InetSocketAddress("127.0.0.1", SERVER_C)));
        serverMap.put(SERVER_D, new Server("d", new InetSocketAddress("127.0.0.1", SERVER_D)));
        serverMap.put(SERVER_E, new Server("e", new InetSocketAddress("127.0.0.1", SERVER_E)));
        serverMap.put(SERVER_F, new Server("f", new InetSocketAddress("127.0.0.1", SERVER_F)));
        serverMap.put(SERVER_G, new Server("g", new InetSocketAddress("127.0.0.1", SERVER_G)));
        random = new Random();
    }

    public void run() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup();
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            int timeout = random.nextInt(10 ) + 1;

                            System.out.println("Setting random timeout to: " + timeout + " seconds");
                            ch.pipeline()
                                    .addLast("idleStateHandler", new IdleStateHandler(0, 0, timeout))
                                    .addLast("clientInboundHandler", new ClientInboundHandler(serverMap))
                                    .addLast("loadBalancerChannelDuplexHandler", new LoadBalancerChannelDuplexHandler(serverMap));
                        }
                    })
                    .option(ChannelOption.SO_BACKLOG, 128)
                    .childOption(ChannelOption.SO_KEEPALIVE, true);

            ChannelFuture f = b.bind(port).sync();

            f.channel().closeFuture().sync();
        } finally {
            workerGroup.shutdownGracefully();
            bossGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        int port = 8080;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        }

        new LoadBalancer(port).run();
    }
}

