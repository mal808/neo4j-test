package com.me.loadbalancer;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Returns server name to client
 */
public class ClientInboundHandler extends ChannelInboundHandlerAdapter {
    private ConcurrentHashMap<Integer, Server> serverMap;

    public ClientInboundHandler(final ConcurrentHashMap<Integer, Server> serverMap){
        this.serverMap = serverMap;
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) {
        Optional<Map.Entry<Integer, Server>> optional = serverMap.entrySet().stream().min((x, y) -> Integer.compare(x.getValue().getCount(), y.getValue().getCount()));
        Server server = optional.isPresent() ? optional.get().getValue() : serverMap.get(LoadBalancer.SERVER_A);
        server.incrementCount();
        server.addConnection(ctx.channel());
        System.out.println("Sending: " + server.getName() + " has " + server.getCount() + " connections.");
        ByteBuf buf = ctx.alloc().buffer(4);
        buf.writeBytes(server.getName().getBytes());
        ctx.writeAndFlush(buf);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
